package com.example.android.bluetoothlegatt;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.IMarker;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

public class PlotLineChart {
    public static final float CHART_VALUE_TEXT_SIZE = 5.0f;
    public static final float CHART_LINE_WIDTH = 2.0f;

    private Context mContext;
    private LineChart mMyLineChart;
    public static XAxis xAxis = null;

    public static LineDataSet lineDataSetX_Acc,lineDataSetY_Acc,lineDataSetZ_Acc;
    public static LineDataSet lineDataSetX_Gyr,lineDataSetY_Gyr,lineDataSetZ_Gyr;

    public PlotLineChart(Context context, LineChart mLineChar) {
        this.mContext = context;
        this.mMyLineChart = mLineChar;
    }

    public void prepareVectorChart() {
        if (!mMyLineChart.isEmpty()) {
            mMyLineChart.clearValues();
        }
        mMyLineChart.getDescription().setText(mContext.getString(R.string.title_accelerometer_vector));
        mMyLineChart.setTouchEnabled(true);
        mMyLineChart.setVisibleXRangeMinimum(5);
        mMyLineChart.setVisibleXRangeMaximum(5);
        // enable scaling and dragging
        mMyLineChart.setDragEnabled(true);
        mMyLineChart.setPinchZoom(true);
        mMyLineChart.setScaleEnabled(true);
        mMyLineChart.setAutoScaleMinMaxEnabled(true);
        mMyLineChart.setDrawGridBackground(false);
        mMyLineChart.setBackgroundColor(Color.WHITE);

        // create marker to display box when values are selected
        MyMarkerView mv = new MyMarkerView(mContext.getApplicationContext(), R.layout.custom_marker_view);

        // Set the marker to the chart
        mv.setChartView(mMyLineChart);
        mMyLineChart.setMarker(mv);

        LineData data = new LineData();
        data.setValueTextColor(Color.WHITE);
        mMyLineChart.setData(data);

        Legend legend = mMyLineChart.getLegend();
        legend.setEnabled(true);

        xAxis = mMyLineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextColor(Color.BLACK);
        xAxis.setDrawGridLines(false);
        xAxis.setAvoidFirstLastClipping(true);

        YAxis leftAxis = mMyLineChart.getAxisLeft();
        leftAxis.setTextColor(Color.BLACK);
        leftAxis.setDrawLabels(true);
        leftAxis.setAxisMinValue(-2.0f);
        leftAxis.setAxisMaxValue(2.0f);
        leftAxis.setLabelCount(6, false);
        leftAxis.setDrawZeroLine(true);

        YAxis rightAxis = mMyLineChart.getAxisRight();
        rightAxis.setEnabled(false);
    }

    public void setLeftAxisMinValue(float minValue){
        YAxis leftAxis = mMyLineChart.getAxisLeft();
        leftAxis.setAxisMinValue(minValue);
    }

    public void setLeftAxisMaxValue(float maxValue){
        YAxis leftAxis = mMyLineChart.getAxisLeft();
        leftAxis.setAxisMaximum(maxValue);
    }

    public void setLabelCount(int labelCount){
        YAxis leftAxis = mMyLineChart.getAxisLeft();
        leftAxis.setLabelCount(labelCount);
    }

    private LineDataSet[] createVectorDataSet() {

        final LineDataSet[] lineDataSets = new LineDataSet[6];

        lineDataSetX_Acc = new LineDataSet(null, "Acc X");
        lineDataSetX_Acc.setAxisDependency(YAxis.AxisDependency.LEFT);
        lineDataSetX_Acc.setColor(ContextCompat.getColor(mContext, R.color.colorRed));
        lineDataSetX_Acc.setHighLightColor(ContextCompat.getColor(mContext, R.color.nordicLake));
        lineDataSetX_Acc.setDrawValues(false);
        lineDataSetX_Acc.setDrawCircles(false);
        lineDataSetX_Acc.setDrawCircleHole(false);
        lineDataSetX_Acc.setValueTextSize(CHART_VALUE_TEXT_SIZE);
        lineDataSetX_Acc.setLineWidth(CHART_LINE_WIDTH);
        lineDataSets[0] = lineDataSetX_Acc;

        lineDataSetY_Acc = new LineDataSet(null, "Acc Y");
        lineDataSetY_Acc.setAxisDependency(YAxis.AxisDependency.LEFT);
        lineDataSetY_Acc.setColor(ContextCompat.getColor(mContext, R.color.colorGreen));
        lineDataSetY_Acc.setHighLightColor(ContextCompat.getColor(mContext, R.color.nordicLake));
        lineDataSetY_Acc.setDrawValues(false);
        lineDataSetY_Acc.setDrawCircles(false);
        lineDataSetY_Acc.setDrawCircleHole(false);
        lineDataSetY_Acc.setValueTextSize(CHART_VALUE_TEXT_SIZE);
        lineDataSetY_Acc.setLineWidth(CHART_LINE_WIDTH);
        lineDataSets[1] = lineDataSetY_Acc;

        lineDataSetZ_Acc = new LineDataSet(null, "Acc Z");
        lineDataSetZ_Acc.setAxisDependency(YAxis.AxisDependency.LEFT);
        lineDataSetZ_Acc.setColor(ContextCompat.getColor(mContext, R.color.colorBlue));
        lineDataSetZ_Acc.setHighLightColor(ContextCompat.getColor(mContext, R.color.nordicLake));
        lineDataSetZ_Acc.setDrawValues(false);
        lineDataSetZ_Acc.setDrawCircles(false);
        lineDataSetZ_Acc.setDrawCircleHole(false);
        lineDataSetZ_Acc.setValueTextSize(CHART_VALUE_TEXT_SIZE);
        lineDataSetZ_Acc.setLineWidth(CHART_LINE_WIDTH);
        lineDataSets[2] = lineDataSetZ_Acc;

        lineDataSetX_Gyr = new LineDataSet(null, "Gyr X");
        lineDataSetX_Gyr.setAxisDependency(YAxis.AxisDependency.LEFT);
        lineDataSetX_Gyr.setColor(ContextCompat.getColor(mContext, R.color.colorYellow));
        lineDataSetX_Gyr.setHighLightColor(ContextCompat.getColor(mContext, R.color.nordicLake));
        lineDataSetX_Gyr.setDrawValues(false);
        lineDataSetX_Gyr.setDrawCircles(false);
        lineDataSetX_Gyr.setDrawCircleHole(false);
        lineDataSetX_Gyr.setValueTextSize(CHART_VALUE_TEXT_SIZE);
        lineDataSetX_Gyr.setLineWidth(CHART_LINE_WIDTH);
        lineDataSets[3] = lineDataSetX_Gyr;

        lineDataSetY_Gyr = new LineDataSet(null, "Gyr Y");
        lineDataSetY_Gyr.setAxisDependency(YAxis.AxisDependency.LEFT);
        lineDataSetY_Gyr.setColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        lineDataSetY_Gyr.setHighLightColor(ContextCompat.getColor(mContext, R.color.nordicLake));
        lineDataSetY_Gyr.setDrawValues(false);
        lineDataSetY_Gyr.setDrawCircles(false);
        lineDataSetY_Gyr.setDrawCircleHole(false);
        lineDataSetY_Gyr.setValueTextSize(CHART_VALUE_TEXT_SIZE);
        lineDataSetY_Gyr.setLineWidth(CHART_LINE_WIDTH);
        lineDataSets[4] = lineDataSetY_Gyr;

        lineDataSetZ_Gyr = new LineDataSet(null, "Gyr Z");
        lineDataSetZ_Gyr.setAxisDependency(YAxis.AxisDependency.LEFT);
        lineDataSetZ_Gyr.setColor(ContextCompat.getColor(mContext, R.color.darkturquoise));
        lineDataSetZ_Gyr.setHighLightColor(ContextCompat.getColor(mContext, R.color.nordicLake));
        lineDataSetZ_Gyr.setDrawValues(false);
        lineDataSetZ_Gyr.setDrawCircles(false);
        lineDataSetZ_Gyr.setDrawCircleHole(false);
        lineDataSetZ_Gyr.setValueTextSize(CHART_VALUE_TEXT_SIZE);
        lineDataSetZ_Gyr.setLineWidth(CHART_LINE_WIDTH);
        lineDataSets[5] = lineDataSetZ_Gyr;

        return lineDataSets;
    }

    public void addVectorEntry(final double[] vectorAcc, final double[] vectorGyr) {

        LineData data = mMyLineChart.getData();

        if (data != null) {
            ILineDataSet setX_Acc = data.getDataSetByIndex(0);
            ILineDataSet setY_Acc = data.getDataSetByIndex(1);
            ILineDataSet setZ_Acc = data.getDataSetByIndex(2);

            ILineDataSet setX_Gyr = data.getDataSetByIndex(3);
            ILineDataSet setY_Gyr = data.getDataSetByIndex(4);
            ILineDataSet setZ_Gyr = data.getDataSetByIndex(5);

            if (setX_Acc == null || setY_Acc == null || setZ_Acc == null) {
                final LineDataSet[] dataSets = createVectorDataSet();
                setX_Acc = dataSets[0];
                setY_Acc = dataSets[1];
                setZ_Acc = dataSets[2];
                setX_Gyr = dataSets[3];
                setY_Gyr = dataSets[4];
                setZ_Gyr = dataSets[5];

                data.addDataSet(setX_Acc);
                data.addDataSet(setY_Acc);
                data.addDataSet(setZ_Acc);
                data.addDataSet(setX_Gyr);
                data.addDataSet(setY_Gyr);
                data.addDataSet(setZ_Gyr);
            }
            data.addEntry(new Entry(setX_Acc.getEntryCount(), (float) vectorAcc[0]), 0);
            data.addEntry(new Entry(setY_Acc.getEntryCount(), (float) vectorAcc[1]), 1);
            data.addEntry(new Entry(setZ_Acc.getEntryCount(), (float) vectorAcc[2]), 2);
            data.addEntry(new Entry(setX_Gyr.getEntryCount(), (float) vectorGyr[0]), 3);
            data.addEntry(new Entry(setY_Gyr.getEntryCount(), (float) vectorGyr[1]), 4);
            data.addEntry(new Entry(setZ_Gyr.getEntryCount(), (float) vectorGyr[2]), 5);
            data.notifyDataChanged();

            mMyLineChart.notifyDataSetChanged();
            mMyLineChart.setVisibleXRangeMaximum(300);
            mMyLineChart.moveViewToX(data.getEntryCount() - 11);
        }
    }

}
