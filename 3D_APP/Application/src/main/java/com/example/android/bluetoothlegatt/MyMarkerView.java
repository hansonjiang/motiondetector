package com.example.android.bluetoothlegatt;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;
import java.util.List;

@SuppressLint("ViewConstructor")
public class MyMarkerView extends MarkerView {

    private final TextView tvValue0;
    public static Chart chart;
    public static Entry mark_entry = null;
    public MyMarkerView(Context context, int layoutResource) {
        super(context, layoutResource);

        tvValue0 =  findViewById(R.id.tv_value0);
    }

    // runs every time the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    @Override
    public void refreshContent(Entry e, Highlight highlight) {
        chart = getChartView();
        mark_entry = e;

        if (e instanceof CandleEntry) {
            CandleEntry ce = (CandleEntry) e;
            tvValue0.setText(Utils.formatNumber(ce.getHigh(), 4, true));
        } else {
            tvValue0.setText(Utils.formatNumber(e.getY(), 4, true));
        }

        LimitLine ll = new LimitLine(mark_entry.getX()+DeviceControlActivity.data_saved_cnt, "");
        ll.setLineColor(Color.RED);
        ll.setLineWidth(1f);

        PlotLineChart.xAxis.removeAllLimitLines();
        PlotLineChart.xAxis.addLimitLine(ll);

        super.refreshContent(e, highlight);
    }

    @Override
    public MPPointF getOffset() {
        return new MPPointF(-(getWidth() / 2), -getHeight());
    }
}
