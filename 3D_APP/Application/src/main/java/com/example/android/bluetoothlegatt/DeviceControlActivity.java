/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.bluetoothlegatt;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.RadioGroup;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.example.android.bluetoothlegatt.MyMarkerView.chart;

import uk.me.berndporr.iirj.Butterworth;

/**
 * For a given BLE device, this Activity provides the user interface to connect, display data,
 * and display GATT services and characteristics supported by the device.  The Activity
 * communicates with {@code BluetoothLeService}, which in turn interacts with the
 * Bluetooth LE API.
 */
public class DeviceControlActivity extends Activity {
    private final static String TAG = DeviceControlActivity.class.getSimpleName();

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";

    private TextView mConnectionState;
    private TextView mDataField;
    private String mDeviceName;
    private String mDeviceAddress;
    private ExpandableListView mGattServicesList;
    private BluetoothLeService mBluetoothLeService;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private boolean mConnected = false;
    private BluetoothGattCharacteristic mNotifyCharacteristic;

    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";

    // BLE Raw data
    private double[] mAccelerometer = new double[3];
    private double[] mGyroscope = new double[3];
    private double[] mMag = new double[3];
    private double[] tcAcc = new double[3];
    private double[] tcGyro = new double[3];
    private double[] tcMag = new double[3];
    private static final float ALPHA = 0.443137f, ALPHA_COM = 1.0f - ALPHA;//20Hz:0.443137f 10Hz:0.61413f
    private static final double dt = (double)(1.0d / 100.0d);
    private double q1 = 1.0d;
    private double q2 = 0.0d;
    private double q3 = 0.0d;
    private double q4 = 0.0d;
    private double Kp = 0.5d;
    private double Ki = 0.001d;
    private double exInt,eyInt,ezInt;
    private double[] linAcc = new double[3];
    private double[] linVel = new double[3];
    private double[] HPLinVel = new double[3];
    private double[] linVel_filter_buffer = new double[3];
    private double[] linVel_filter = new double[3];
    private int[] vel_cnt = new int[3];
    private int[] mag_cnt = new int[3];
    private double[] linPos = new double[3];
    private double[] linPos_last = new double[3];
    private double[] linAcc_last = new double[3];
    private int[] linAcc_K_cnt = new int[3];
    private double[] LPLinPos = new double[3];
    private double[] tcMag_Old = new double[3];
    private double[] position_Old = new double[3];
    private double[] correction_position = new double[3];
    private double[] linAcc_bias = new double[3];
    private double rot_mtx_0_0,rot_mtx_0_1,rot_mtx_0_2,rot_mtx_1_0,rot_mtx_1_1,rot_mtx_1_2,rot_mtx_2_0,rot_mtx_2_1,rot_mtx_2_2;
    private double[] gyro_pi = new double[3];
    private int[] action_state = new int[3];
    private int[] bias_state = new int[3];
    private int[] position_back_en = new int[3];
    private int[] bias_delay_cnt = new int[3];
    private int[] linAcc_zero_delay_cnt = new int[3];
    private int[] linVel_filter_cnt = new int[3];
    private int[] linPos_filter_cnt = new int[3];
    private int[] zero_vel_state_down_cnt = new int[3];
    private int[] zero_vel_state_up_cnt = new int[3];
    private boolean[] zero_vel_state = new boolean[3];
    private int[] linAcc_bias_state = new int[2];
    private double[] debug = new double[3];
    private boolean initial_mag = true;
    private double[] initial_mag_value = new double[3];
    private int reset_position_cnt;
    private double t;
    private double yaw,roll,pitch;
    private double sin_yaw;
    private double cos_yaw;
    private double[] max_linPos = new double[3];
    private double[] bias_gyro_level = {15.0d,15.0d,15.0d};
    private double[] bias_linVel_level = {10.0d,10.0d,10.0d};
    private double[] acc_filter = new double[3];
    private double[] err_sum = new double[3];
    private int[] cal_rst_cnt = new int[3];
    private double[] em_filter_buffer = new double[3];
    private double[] em_filter = new double[3];

    // Plot line chart
    private LineChart mAcceLineChart;
    private PlotLineChart mPlotLineChart;
    private Butterworth butterworth_Mag_X;
    private Butterworth butterworth_Mag_Y;
    private Butterworth butterworth_Mag_Z;
    static double fs = 100;
    static double fc_mag = 1.0;
    static int order = 1;
    //Boolean control line chart show
    private boolean aBoolean_LineChart = true;

    //Save File
    private String strFileName = "TempTraining.csv";
    File outFile;
    public FileOutputStream fileOutputStream_txt = null;

    public static int data_saved_cnt = 0;

    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                updateConnectionState(R.string.connected);
                invalidateOptionsMenu();
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                updateConnectionState(R.string.disconnected);
                invalidateOptionsMenu();
                clearUI();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                displayGattServices(mBluetoothLeService.getSupportedGattServices());
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                mAccelerometer[0] = intent.getExtras().getDouble(BluetoothLeService.EXTRA_DATA_ACCELEROMETER_X);
                mAccelerometer[1] = intent.getExtras().getDouble(BluetoothLeService.EXTRA_DATA_ACCELEROMETER_Y);
                mAccelerometer[2] = intent.getExtras().getDouble(BluetoothLeService.EXTRA_DATA_ACCELEROMETER_Z);

                mGyroscope[0] = intent.getExtras().getDouble(BluetoothLeService.EXTRA_DATA_GYROSCOPE_X);
                mGyroscope[1] = intent.getExtras().getDouble(BluetoothLeService.EXTRA_DATA_GYROSCOPE_Y);
                mGyroscope[2] = intent.getExtras().getDouble(BluetoothLeService.EXTRA_DATA_GYROSCOPE_Z);

                if(intent.getExtras().getDouble(BluetoothLeService.EXTRA_DATA_MAG_X) != 0 && intent.getExtras().getDouble(BluetoothLeService.EXTRA_DATA_MAG_Y) != 0 && intent.getExtras().getDouble(BluetoothLeService.EXTRA_DATA_MAG_Z) != 0) {
                    mMag[0] = intent.getExtras().getDouble(BluetoothLeService.EXTRA_DATA_MAG_X);
                    mMag[1] = intent.getExtras().getDouble(BluetoothLeService.EXTRA_DATA_MAG_Y);
                    mMag[2] = intent.getExtras().getDouble(BluetoothLeService.EXTRA_DATA_MAG_Z); //End
                }
                acc_filter[0] = ALPHA_COM * (mAccelerometer[0] - acc_filter[0]) + acc_filter[0];
                acc_filter[1] = ALPHA_COM * (mAccelerometer[1] - acc_filter[1]) + acc_filter[1];
                acc_filter[2] = ALPHA_COM * (mAccelerometer[2] - acc_filter[2]) + acc_filter[2];
                //Cal_Quatern(mGyroscope[0],mGyroscope[1],mGyroscope[2],mAccelerometer[0],mAccelerometer[1],mAccelerometer[2]);
                displayData(String.format("Accel: %7.4f, %7.4f, %7.4f, Gyro: %7.4f, %7.4f, %7.4f", debug[0], debug[1], debug[2], mMag[0], mMag[1], mMag[2]));
                if(!aBoolean_LineChart) {
                    update(mGyroscope[0],mGyroscope[1],mGyroscope[2],acc_filter[0],acc_filter[1],acc_filter[2],mMag[0],mMag[1],mMag[2]);
                    mPlotLineChart.addVectorEntry(mMag,debug);
                    /*if(debug[0] == 10) {
                        debug[1] = 1;
                    }
                    debug[0] = 0;*/
                }
            }
        }
    };

    private void update(double gx, double gy, double gz, double ax, double ay, double az, double mx, double my, double mz){
        double recipNorm;
        double recipNorm_mag;
        //double mh1,mh2,mh3,mh4;
        //double h1,h2,h3,h4;
        //double b2,b4;
        double hx,hy,hz;
        double vx, vy, vz;
        double wx, wy, wz;
        double ex = 0, ey = 0, ez = 0;
        double emx = 0, emy = 0, emz = 0;
        double qDot1, qDot2, qDot3, qDot4;
        double invSampleFreq = (double)1.0d / 100.0d;
        double K;
        double K_init = 10.0d;
        double t_init = 300.0d;

        // Convert gyroscope degrees/sec to radians/sec
        gx = (double)(gx*0.0174533d);
        gy = (double)(gy*0.0174533d);
        gz = (double)(gz*0.0174533d);

        if(!((ax == 0.0d) && (ay == 0.0d) && (az == 0.0d))) {
            // Normalise accelerometer measurement
            recipNorm = (double) Math.sqrt(ax * ax + ay * ay + az * az);
            ax = (double)(ax/recipNorm);
            ay = (double)(ay/recipNorm);
            az = (double)(az/recipNorm);
            if(!((mx == 0.0d) && (my == 0.0d) && (mz == 0.0d))) {
                // Normalise magnetic measurement
                recipNorm_mag = (double)Math.sqrt(mx * mx + my * my + mz * mz);
                mx = (double)(mx/recipNorm_mag);
                my = (double)(my/recipNorm_mag);
                mz = (double)(mz/recipNorm_mag);

                //Reference direction of Earth's magnetic feild
                //mh1 = (double)(mx*q2+my*q3+mz*q4);
                //mh2 = (double)(mx*q1-my*q4+mz*q3);
                //mh3 = (double)(mx*q4+my*q1-mz*q2);
                //mh4 = (double)(-mx*q3+my*q2+mz*q1);

                //h1 = (double)(q1*mh1-q2*mh2-q3*mh3-q4*mh4);
                //h2 = (double)(q1*mh2+q2*mh1+q3*mh4-q4*mh3);
                //h3 = (double)(q1*mh3-q2*mh4+q3*mh1+q4*mh2);
                //h4 = (double)(q1*mh4+q2*mh3-q3*mh2+q4*mh1);

                //b2 = (double)Math.sqrt(h2 * h2 + h3 * h3);
                //b4 = h4;

                // Estimated direction of gravity and magnetic flux
                vx = (double)(2.0d * (q2 * q4 - q1 * q3));
                vy = (double)(2.0d * (q1 * q2 + q3 * q4));
                vz = (double)(2.0d * (q1 * q1 + q4 * q4) - 1);

                //wx = (double) (2.0d*b2*(0.5d - q3*q3 - q4*q4) + 2.0d*b4*(q2*q4 - q1*q3));
                //wy = (double) (2.0d*b2*(q2*q3 - q1*q4) + 2.0d*b4*(q1*q2 + q3*q4));
                //wz = (double) (2.0d*b2*(q1*q3 + q2*q4) + 2.0d*b4*(0.5d - q2*q2 - q3*q3));

                wx = (double)(2.0d * (q2 * q3 + q1 * q4));
                wy = (double)(2.0d * (q1 * q1 + q3 * q3) - 1);
                wz = (double)(2.0d * (q3 * q4 - q1 * q2));

                /*wx = (double)(1 - 2.0d * (q3 * q3 + q4 * q4));
                wy = (double)(2.0d * (q2 * q3 - q1 * q4));
                wz = (double)(2.0d * (q2 * q4 + q1 * q3));*/

                // Error is sum of cross product between estimated direction and measured direction of field
                ex = (double)(ay*vz - az*vy);
                ey = (double)(az*vx - ax*vz);
                ez = (double)(ax*vy - ay*vx);

                hx = (double)(ay*mz - az*my);
                hy = (double)(az*mx - ax*mz);
                hz = (double)(ax*my - ay*mx);

                if(recipNorm_mag > 10.0d && recipNorm_mag < 20.0d) {
                    emx = (double)(hy*wz - hz*wy);
                    emy = (double)(hz*wx - hx*wz);
                    emz = (double)(hx*wy - hy*wx);
                } else {
                    emx = 0.0d;
                    emy = 0.0d;
                    emz = 0.0d;
                }
                em_filter_buffer[0] = emx + em_filter_buffer[0] - em_filter_buffer[0]/(1<<4);
                em_filter[0] = em_filter_buffer[0]/(1<<4);

                if(Math.abs(em_filter[0]) > 0.35d) {
                    emx = 0.0d;
                }

                em_filter_buffer[1] = emy + em_filter_buffer[1] - em_filter_buffer[1]/(1<<4);
                em_filter[1] = em_filter_buffer[1]/(1<<4);

                if(Math.abs(em_filter[1]) > 0.35d) {
                    emy = 0.0d;
                }

                em_filter_buffer[2] = emz + em_filter_buffer[2] - em_filter_buffer[2]/(1<<4);
                em_filter[2] = em_filter_buffer[2]/(1<<4);

                if(Math.abs(em_filter[2]) > 0.35d) {
                    emz = 0.0d;
                }
                
                exInt += ex * invSampleFreq;
                eyInt += ey * invSampleFreq;
                ezInt += ez * invSampleFreq;

                if(t < t_init) {
                    t += 1;
                    K = Kp + (K_init - Kp) * ((t_init - t)/t_init);
                    if((int)t == (int)(t_init)) {
                        yaw = Math.atan2(2*(q1*q4+q2*q3),1-2*(q3*q3 + q4*q4));
                        sin_yaw = Math.sin(yaw);
                        cos_yaw = Math.cos(yaw);
                    }
                } else {
                    K = Kp;
                }

                err_sum[0] = ex + emx;
                err_sum[1] = ey + emy;
                err_sum[2] = ez + emz;

                gx = (double)(gx + K * err_sum[0]);
                gy = (double)(gy + K * err_sum[1]);
                gz = (double)(gz + K * err_sum[2]);

                // Rate of change of quaternion from gyroscope
                qDot1 = (double)(0.5d * (-q2 * gx - q3 * gy - q4 * gz));
                qDot2 = (double)(0.5d * (q1 * gx + q3 * gz - q4 * gy));
                qDot3 = (double)(0.5d * (q1 * gy - q2 * gz + q4 * gx));
                qDot4 = (double)(0.5d * (q1 * gz + q2 * gy - q3 * gx));

                // Integrate rate of change of quaternion to yield quaternion
                q1 = (double)(q1 + qDot1 * invSampleFreq);
                q2 = (double)(q2 + qDot2 * invSampleFreq);
                q3 = (double)(q3 + qDot3 * invSampleFreq);
                q4 = (double)(q4 + qDot4 * invSampleFreq);

                // Normalise quaternion
                recipNorm = (double)Math.sqrt(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);
                q1 = (double)(q1/recipNorm);
                q2 = (double)(q2/recipNorm);
                q3 = (double)(q3/recipNorm);
                q4 = (double)(q4/recipNorm);
            }
        }
        quatern2rotMat(q1,q2,q3,q4);

        yaw = Math.atan2(2*(q1*q4+q2*q3),1-2*(q3*q3 + q4*q4)) / Math.PI * 180;
        pitch = Math.asin(2*(q1*q3-q2*q4));
        roll = Math.atan2(2*(q1*q2+q3*q4),1-2*(q2*q2 + q3*q3));

        if(t >= t_init) {

            tcAcc[0] = (double)(rot_mtx_0_0*acc_filter[0] + rot_mtx_0_1*acc_filter[1] + rot_mtx_0_2*acc_filter[2]);
            tcAcc[1] = (double)(rot_mtx_1_0*acc_filter[0] + rot_mtx_1_1*acc_filter[1] + rot_mtx_1_2*acc_filter[2]);
            tcAcc[2] = (double)(rot_mtx_2_0*acc_filter[0] + rot_mtx_2_1*acc_filter[1] + rot_mtx_2_2*acc_filter[2]);

            //tcAcc[0] = (double)(rot_mtx_0_0*mAccelerometer[0] + rot_mtx_0_1*mAccelerometer[1] + rot_mtx_0_2*mAccelerometer[2]);
            //tcAcc[1] = (double)(rot_mtx_1_0*mAccelerometer[0] + rot_mtx_1_1*mAccelerometer[1] + rot_mtx_1_2*mAccelerometer[2]);
            //tcAcc[2] = (double)(rot_mtx_2_0*mAccelerometer[0] + rot_mtx_2_1*mAccelerometer[1] + rot_mtx_2_2*mAccelerometer[2]);

            tcMag[0] = (double)(rot_mtx_0_0*mMag[0] + rot_mtx_0_1*mMag[1] + rot_mtx_0_2*mMag[2]);
            tcMag[1] = (double)(rot_mtx_1_0*mMag[0] + rot_mtx_1_1*mMag[1] + rot_mtx_1_2*mMag[2]);
            tcMag[2] = (double)(rot_mtx_2_0*mMag[0] + rot_mtx_2_1*mMag[1] + rot_mtx_2_2*mMag[2]);

            linAcc[0] = tcAcc[0]*cos_yaw + tcAcc[1]*sin_yaw;
            linAcc[1] = -tcAcc[0]*sin_yaw + tcAcc[1]*cos_yaw;
            linAcc[2] = (double)(tcAcc[2] - 1.0d);

            if(initial_mag) {
                initial_mag_value[0] = tcMag[0];
                initial_mag_value[1] = tcMag[1];
                initial_mag_value[2] = tcMag[2];
            } else {
                if((Math.abs(tcMag[0] - initial_mag_value[0]) + Math.abs(tcMag[1] - initial_mag_value[1]) + Math.abs(tcMag[2] - initial_mag_value[2]))< 3.0f) {
                    if(reset_position_cnt > 10) {
                        LPLinPos[0] = 0.0f;
                        LPLinPos[1] = 0.0f;
                        LPLinPos[2] = 0.0f;
                    } else {
                        reset_position_cnt++;
                    }
                } else {
                    reset_position_cnt = 0;
                }
            }

            if(linVel_filter_cnt[0] >= 200) {
                if(Math.abs(linAcc[0]) < 0.2d && Math.abs(linAcc[2]) < 0.2d && linAcc_bias_state[0] == 1) {
                    linAcc_bias[0] = 0.11d;
                    linAcc_bias_state[0] = 0;
                } else {
                    if(Math.abs(linAcc[0]) > 0.35d && Math.abs(linAcc[2]) > 0.35d) {
                        linAcc_bias_state[0] = 1;
                    }
                    else if(Math.abs(Math.abs(linAcc[0]) - Math.abs(linAcc[2])) > 0.7d) {
                        linAcc_bias[0] = 0.0d;
                        linAcc_bias_state[0] = 0;
                    }
                }

                if(Math.abs(linAcc[1]) < 0.2d && Math.abs(linAcc[2]) < 0.2d && linAcc_bias_state[1] == 1) {
                    linAcc_bias[1] = 0.11d;
                    linAcc_bias_state[1] = 0;
                } else {
                    if(Math.abs(linAcc[1]) > 0.35d && Math.abs(linAcc[2]) > 0.35d) {
                        linAcc_bias_state[1] = 1;
                    }
                    else if(Math.abs(Math.abs(linAcc[1]) - Math.abs(linAcc[2])) > 0.6d) {
                        linAcc_bias[1] = 0.0d;
                        linAcc_bias_state[1] = 0;
                    }
                }
            }


            for(int i = 0 ;i < linVel.length; i++) {
                /*if(Math.abs(linAcc[i]) < 0.03d) {
                    if(zero_vel_state[i] == true) {
                        zero_vel_state[i] = false;
                        action_state[i] = 0;
                    }
                }*/
                if(Math.abs(linAcc[i]) < Math.abs(linAcc_last[i]) && zero_vel_state[i] == true) {
                    cal_rst_cnt[i] = 0;
                    zero_vel_state_up_cnt[i] = 0;
                    if(zero_vel_state_down_cnt[i] >= 5) {
                        zero_vel_state[i] = false;
                        zero_vel_state_down_cnt[i] = 0;
                    } else {
                        zero_vel_state_down_cnt[i]++;
                    }
                }
                else if(Math.abs(linAcc[i]) > Math.abs(linAcc_last[i])) {
                    cal_rst_cnt[i] = 0;
                    zero_vel_state_down_cnt[i] = 0;
                    if(zero_vel_state_up_cnt[i] >= 10) {
                        zero_vel_state[i] = true;
                        action_state[i] = 1;
                        zero_vel_state_up_cnt[i] = 0;
                    } else {
                        zero_vel_state_up_cnt[i]++;
                    }
                }
                else {
                    zero_vel_state_up_cnt[i] = 0;
                    zero_vel_state_down_cnt[i] = 0;
                    if(Math.abs(linAcc[i]) < 0.2d) {
                        if(zero_vel_state[i] == false) {
                            if(action_state[i] == 0) {
                                linVel[i] = 0.0d;
                                linAcc[i] = 0.0d;

                                vel_cnt[i] = 0;
                                linVel_filter_buffer[i] = 0.0d;
                                linVel_filter[i] = 0.0d;
                                linVel_filter_cnt[i] = 0;
                                linPos_filter_cnt[i] = 0;
                                mag_cnt[i] = 0;
                                bias_state[i] = 0;
                                bias_delay_cnt[i] = 0;
                                HPLinVel[i] = 0;
                                linAcc_bias[i] = 0.0d;
                                linAcc_zero_delay_cnt[i] = 0;
                                max_linPos[i] = 0.0d;
                            }
                            if(Math.abs(linPos[i]) < (bias_linVel_level[i]*2.0d)) {
                                if(cal_rst_cnt[i] > 2) {

                                    action_state[i] = 0;
                                    linVel[i] = 0.0d;
                                    linAcc[i] = 0.0d;

                                    vel_cnt[i] = 0;
                                    linVel_filter_buffer[i] = 0.0d;
                                    linVel_filter[i] = 0.0d;
                                    linVel_filter_cnt[i] = 0;
                                    linPos_filter_cnt[i] = 0;
                                    mag_cnt[i] = 0;
                                    bias_state[i] = 0;
                                    bias_delay_cnt[i] = 0;
                                    HPLinVel[i] = 0;
                                    linAcc_bias[i] = 0.0d;
                                    linAcc_zero_delay_cnt[i] = 0;
                                    max_linPos[i] = 0.0d;
                                } else {
                                    cal_rst_cnt[i]++;
                                }
                            } else {
                                cal_rst_cnt[i] = 0;
                            }
                        }
                    } else {
                        action_state[i] = 1;
                        cal_rst_cnt[i] = 0;
                    }
                }

                if(Math.abs(linAcc[i]) > 0.03d || action_state[i] == 1) {

                    linVel[i] = linVel[i] + (linAcc_last[i]/* + linAcc_bias[i] */+ (linAcc[i]-linAcc_last[i])/2.0f) * 9.81f;

                    linVel_filter_buffer[i] = linVel[i] + linVel_filter_buffer[i] - linVel_filter_buffer[i]/(1<<4);
                    linVel_filter[i] = linVel_filter_buffer[i]/(1<<4);

                    /*if(action_state[i] == 0) {
                        //Record the last tilt-compensated magnetic value
                        tcMag_Old[i] = tcMag[i];
                        //Record the last position
                        position_Old[i] = LPLinPos[i];
                        debug[1] = 10;
                    } else {
                        debug[2] = Math.abs(tcMag[0] - tcMag_Old[0]) + Math.abs(tcMag[1] - tcMag_Old[1]) + Math.abs(tcMag[2] - tcMag_Old[2]);
                        if((Math.abs(tcMag[0] - tcMag_Old[0]) + Math.abs(tcMag[1] - tcMag_Old[1]) + Math.abs(tcMag[2] - tcMag_Old[2]))< 2.0d && debug[1] == 0) {
                            debug[0] = 10;
                            LPLinPos[i] = position_Old[i];
                        } else {
                            if((Math.abs(tcMag[0] - tcMag_Old[0]) + Math.abs(tcMag[1] - tcMag_Old[1]) + Math.abs(tcMag[2] - tcMag_Old[2]))> 5.0d) {
                                debug[1] = 0;
                            }
                        }
                    }*/

                    if(bias_delay_cnt[i] > 20) {
                        Kp = 0.5d;
                        if(zero_vel_state[i] == false) {
                            if(Math.abs(linVel[i]) < bias_linVel_level[i]) {
                                if(Math.abs(linAcc[i]) < 0.35d) {
                                    bias_state[i] = 1;
                                } else {
                                    bias_state[i] = 0;
                                }
                            }
                        } else {
                            bias_state[i] = 0;
                        }
                    } else {
                        bias_delay_cnt[i]++;
                    }

                    if(bias_state[i] == 1) {
                        if(Math.abs(linAcc[i]) < 0.05d) {
                            if(linAcc_zero_delay_cnt[i] > 10) {
                                bias_state[i] = 0;
                                linVel[i] = 0.0d;
                                linVel_filter_buffer[i] = 0.0d;
                                linVel_filter[i] = 0.0d;
                                HPLinVel[i] = 0.0d;
                            } else {
                                linAcc_zero_delay_cnt[i]++;
                            }
                        } else {
                            linAcc_zero_delay_cnt[i] = 0;
                        }
                        HPLinVel[i] = linVel[i];
                        if(Math.abs(mGyroscope[0]) >= bias_gyro_level[i] || Math.abs(mGyroscope[1]) >= bias_gyro_level[i] || Math.abs(mGyroscope[2]) >= bias_gyro_level[i]) {
                            bias_state[i] = 0;
                            HPLinVel[i] = 0.0d;
                            linAcc_zero_delay_cnt[i] = 0;
                            //bias_delay_cnt[i] = 0;
                        } else {
                            if(Math.abs(linAcc[i] - linAcc_last[i]) < 0.01d && zero_vel_state[i] == false) {
                                bias_state[i] = 0;
                                linVel[i] = 0.0d;
                                linVel_filter_buffer[i] = 0.0d;
                                linVel_filter[i] = 0.0d;
                                HPLinVel[i] = 0.0d;
                            }
                        }
                    } else {
                        HPLinVel[i] = 0.0d;
                    }

                    if(bias_delay_cnt[i] > 20) {
                        if(Math.abs(mGyroscope[0]) < bias_gyro_level[i] && Math.abs(mGyroscope[1]) < bias_gyro_level[i] && Math.abs(mGyroscope[2]) < bias_gyro_level[i]) {
                            if(Math.abs(linAcc[0]) <= 0.05d && Math.abs(linAcc[1]) <= 0.05d && Math.abs(linAcc[2]) <= 0.05d) {
                                if(vel_cnt[i] > 7) {
                                    if(Math.abs(linPos[i]) > 10.0d) {
                                        if(Math.abs(linPos[i] - linPos_last[i]) < 2.0d) {
                                            LPLinPos[i] = correction_position[i];
                                        }
                                    }
                                    action_state[i] = 0;
                                    vel_cnt[i] = 0;
                                    linVel[i] = 0;
                                    linVel_filter_buffer[i] = 0.0d;
                                    linVel_filter[i] = 0.0d;
                                    linVel_filter_cnt[i] = 0;
                                    linPos_filter_cnt[i] = 0;
                                    mag_cnt[i] = 0;
                                    bias_state[i] = 0;
                                    bias_delay_cnt[i] = 0;
                                    HPLinVel[i] = 0;
                                    linAcc_bias[i] = 0.0d;
                                    linAcc_zero_delay_cnt[i] = 0;
                                    max_linPos[i] = 0.0d;
                                } else {
                                    vel_cnt[i]++;
                                }
                            } else {
                                vel_cnt[i] = 0;
                            }
                        } else {
                            vel_cnt[i] = 0;
                        }
                    }

                    if(linVel_filter_cnt[i] < 200) {
                        linVel_filter_cnt[i]++;
                        linVel_filter[i] = 0.0d;
                    } else if(HPLinVel[i] != 0.0d) {
                        linVel_filter[i] = 0.0d;
                    }
                } else {
                    HPLinVel[i] = 0.0d;
                }

                linPos_last[i] = linPos[i];

                linPos[i] = (double)(linVel[i] - HPLinVel[i] - linVel_filter[i]);

                if(Math.abs(linPos[i]) > Math.abs(max_linPos[i])) {
                    max_linPos[i] = linPos[i];
                    bias_gyro_level[i] = Math.abs(max_linPos[i]);
                    bias_linVel_level[i] = Math.abs(max_linPos[i]/4.0d);
                    if(bias_gyro_level[i] < 15.0d) bias_gyro_level[i] = 15.0d;
                    if(bias_linVel_level[i] < 10.0d) bias_linVel_level[i] = 10.0d;
                }

                //if(linVel_filter_cnt[i] >= 100) {
                    if(Math.abs(linPos[i]) < 5.0d) {
                        linPos_filter_cnt[i] = 0;
                    }
                    if(linPos_filter_cnt[i] > 100) {
                        linPos[i] = 0.0d;
                        linVel[i] = 0.0d;
                        HPLinVel[i] = 0.0d;
                        linVel_filter[i] = 0.0d;
                    } else {
                        linPos_filter_cnt[i]++;
                    }
                //}

                //if(debug[0] == 0){
                    LPLinPos[i] = (double)(LPLinPos[i] + linPos[i]*dt);
                //}

                if(LPLinPos[i] > 50)LPLinPos[i] = 50;
                else if(LPLinPos[i] < -50) LPLinPos[i] = -50;

                if(Math.abs(linAcc_last[i] - linAcc[i]) < 0.01d && Math.abs(linAcc[i]) > 0.03d){
                    if(linAcc_K_cnt[i] > 10){
                        Kp = 2.5d;
                    } else {
                        linAcc_K_cnt[i]++;
                    }
                } else {
                    linAcc_K_cnt[i] = 0;
                }

                if(action_state[i] == 1) {
                    if(Math.abs(linPos[i]) < 5.0d) {
                        correction_position[i] = LPLinPos[i];
                    }
                }
                linAcc_last[i] = linAcc[i];
            }
            initial_mag = false;

            /*debug[0] = bias_state[1] * 150;
            debug[1] = linAcc[1] * 200.0d;
            if(zero_vel_state[1] == true) {
                debug[2] = 0;
            } else {
                debug[2] = 100;
            }*/

        }
    }

    private void Cal_Quatern(double gx, double gy, double gz, double ax, double ay, double az){
        double recipNorm;
        double qDot1, qDot2, qDot3, qDot4;
        double invSampleFreq = 1.0d / 100.0d;
        double Kp = 1.0d;
        double Ki = 0.0d;
        double vx, vy, vz;
        double ex, ey, ez;

        // Convert gyroscope degrees/sec to radians/sec
        gx *= 0.0174533d;
        gy *= 0.0174533d;
        gz *= 0.0174533d;

        gyro_pi[0] = gx;
        gyro_pi[1] = gy;
        gyro_pi[2] = gz;
        // Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
        if(!((ax == 0.0d) && (ay == 0.0d) && (az == 0.0d))) {

            // Normalise accelerometer measurement
            recipNorm = (float)Math.sqrt(ax * ax + ay * ay + az * az);
            ax /= recipNorm;
            ay /= recipNorm;
            az /= recipNorm;

            // Estimated direction of gravity and magnetic flux
            vx = 2.0d * (q2 * q4 - q1 * q3);
            vy = 2.0d * (q1 * q2 + q3 * q4);
            vz = q1 * q1 - q2 * q2 - q3 * q3 + q4 * q4;

            // Error is sum of cross product between estimated direction and measured direction of field
            ex = ay*vz - az*vy;
            ey = az*vx - ax*vz;
            ez = ax*vy - ay*vx;

            // integral error scaled integral gain
            exInt = exInt + ex * invSampleFreq;
            eyInt = eyInt + ey * invSampleFreq;
            ezInt = ezInt + ez * invSampleFreq;

            // adjusted gyroscope measurements
            gx = gx + Kp * ex + Ki * exInt;
            gy = gy + Kp * ey + Ki * eyInt;
            gz = gz + Kp * ez + Ki * ezInt;

            // Rate of change of quaternion from gyroscope
            qDot1 = 0.5d * (-q2 * gx - q3 * gy - q4 * gz);
            qDot2 = 0.5d * (q1 * gx + q3 * gz - q4 * gy);
            qDot3 = 0.5d * (q1 * gy - q2 * gz + q4 * gx);
            qDot4 = 0.5d * (q1 * gz + q2 * gy - q3 * gx);

            // Integrate rate of change of quaternion to yield quaternion
            q1 += qDot1 * invSampleFreq;
            q2 += qDot2 * invSampleFreq;
            q3 += qDot3 * invSampleFreq;
            q4 += qDot4 * invSampleFreq;

            // Normalise quaternion
            recipNorm = (float)Math.sqrt(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);
            q1 /= recipNorm;
            q2 /= recipNorm;
            q3 /= recipNorm;
            q4 /= recipNorm;

            quatern2rotMat(q1,q2,q3,q4);

            tcAcc[0] = rot_mtx_0_0*mAccelerometer[0] + rot_mtx_0_1*mAccelerometer[1] + rot_mtx_0_2*mAccelerometer[2];
            tcAcc[1] = rot_mtx_1_0*mAccelerometer[0] + rot_mtx_1_1*mAccelerometer[1] + rot_mtx_1_2*mAccelerometer[2];
            tcAcc[2] = rot_mtx_2_0*mAccelerometer[0] + rot_mtx_2_1*mAccelerometer[1] + rot_mtx_2_2*mAccelerometer[2];

            linAcc[0] = tcAcc[0];
            linAcc[1] = tcAcc[1];
            linAcc[2] = (tcAcc[2] - 1.0d);

            for(int i = 0 ;i < linVel.length; i++) {
                if(Math.abs(linAcc[i]) > 0.03d || action_state[i] == 1) {
                    linVel[i] = (double)(linVel[i] + linAcc[i] * 9.81d);

                    if(bias_delay_cnt[i] > 15) {
                        if(Math.abs(linVel[i]) < 2.0d) {
                            if(Math.abs(linAcc[i]) < 0.5d) {
                                bias_state[i] = 1;
                            } else {
                                bias_state[i] = 0;
                            }
                        }

                        if(bias_state[i] == 1) {
                            if(Math.abs(linAcc[i]) > 0.5d) {
                                bias_state[i] = 0;
                            } else {
                                HPLinVel[i] = linVel[i];
                            }
                        } else {
                            HPLinVel[i] = 0.0d;
                        }
                    } else {
                        bias_delay_cnt[i]++;
                    }

                    action_state[i] = 1;

                    if(Math.abs(mGyroscope[0]) < 9.0d && Math.abs(mGyroscope[1]) < 9.0d && Math.abs(mGyroscope[2]) < 9.0d) {
                        if(vel_cnt[i] > 10) {
                            action_state[i] = 0;
                            vel_cnt[i] = 0;
                            linVel[i] = 0;
                            mag_cnt[i] = 0;
                            bias_state[i] = 0;
                            bias_delay_cnt[i] = 0;
                            HPLinVel[i] = 0;
                        } else {
                            vel_cnt[i]++;
                        }
                    } else {
                        vel_cnt[i] = 0;
                    }
                } else {
                    HPLinVel[i] = 0;
                }

                linPos[i] = (double)(linVel[i] - HPLinVel[i]);

                LPLinPos[i] = (double)(LPLinPos[i] + linPos[i]*dt);
                if(LPLinPos[i] > 50)LPLinPos[i] = 50;
                else if(LPLinPos[i] < -50) LPLinPos[i] = -50;
            }
        }
    }
    private void quatern2rotMat(double q1,double q2,double q3,double q4){
        rot_mtx_0_0 = (double)(1.0d - 2.0d * Math.pow(q3,2) - 2.0d * Math.pow(q4,2));
        rot_mtx_0_1 = (double)(2.0d * (q2 * q3 - q1 * q4));
        rot_mtx_0_2 = (double)(2.0d * (q1 * q3 + q2 * q4));
        rot_mtx_1_0 = (double)(2.0d * (q2 * q3 + q1 * q4));
        rot_mtx_1_1 = (double)(1.0d - 2.0d * Math.pow(q2,2) - 2.0d * Math.pow(q4,2));
        rot_mtx_1_2 = (double)(2.0d * (q3 * q4 - q1 * q2));
        rot_mtx_2_0 = (double)(2.0d * (q2 * q4 - q1 * q3));
        rot_mtx_2_1 = (double)(2.0d * (q3 * q4 + q1 *q2));
        rot_mtx_2_2 = (double)(1.0d - 2.0d * Math.pow(q2,2) - 2.0d * Math.pow(q3,2));
    }

    // If a given GATT characteristic is selected, check for supported features.  This sample
    // demonstrates 'Read' and 'Notify' features.  See
    // http://d.android.com/reference/android/bluetooth/BluetoothGatt.html for the complete
    // list of supported characteristic features.
    private final ExpandableListView.OnChildClickListener servicesListClickListner =
            new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                            int childPosition, long id) {
                    if (mGattCharacteristics != null) {
                        final BluetoothGattCharacteristic characteristic =
                                mGattCharacteristics.get(groupPosition).get(childPosition);
                        final int charaProp = characteristic.getProperties();
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                            // If there is an active notification on a characteristic, clear
                            // it first so it doesn't update the data field on the user interface.
                            if (mNotifyCharacteristic != null) {
                                mBluetoothLeService.setCharacteristicNotification(
                                        mNotifyCharacteristic, false);
                                mNotifyCharacteristic = null;
                            }
                            mBluetoothLeService.readCharacteristic(characteristic);
                        }
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                            mNotifyCharacteristic = characteristic;
                            mBluetoothLeService.setCharacteristicNotification(
                                    characteristic, true);
                        }
                        return true;
                    }
                    return false;
                }
    };

    private void clearUI() {
        mGattServicesList.setAdapter((SimpleExpandableListAdapter) null);
        mDataField.setText(R.string.no_data);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gatt_services_characteristics);

        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

        // Sets up UI references.
        ((TextView) findViewById(R.id.device_address)).setText(mDeviceAddress);
        mGattServicesList = (ExpandableListView) findViewById(R.id.gatt_services_list);
        mGattServicesList.setOnChildClickListener(servicesListClickListner);
        mConnectionState = (TextView) findViewById(R.id.connection_state);
        mDataField = (TextView) findViewById(R.id.data_value);

        getActionBar().setTitle(mDeviceName);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);

        // Johnny My Data
        mAccelerometer = new double[3];
        mGyroscope = new double[3];
        mAcceLineChart = (LineChart) findViewById(R.id.acceLineChart);
        mPlotLineChart = new PlotLineChart(getApplicationContext(), mAcceLineChart);
        mPlotLineChart.prepareVectorChart();

        // Hanson Add
        ToggleButton mToggleButton;
        mToggleButton=(ToggleButton)findViewById(R.id.mToggleButton);

        mToggleButton.setTextOff("STOP"); //設定未選取時的文字
        mToggleButton.setTextOn("START"); //設定選取時的文字
        mToggleButton.setChecked(true);	//設定按紐狀態 - true:選取, false:未選取

        mToggleButton.setOnCheckedChangeListener(new toggleButton_OnCheckedChangeListener()); //設定按紐狀態監聽器

        final File dir = new File(Environment.getExternalStorageDirectory() + "/"+ Environment.DIRECTORY_DOWNLOADS);
        outFile = new File(dir, strFileName);
        try {
            fileOutputStream_txt = new FileOutputStream(outFile, true);
        }catch (Exception e){
            e.printStackTrace();
        }

        CheckBox satView_Acc = (CheckBox) findViewById(R.id.cbox_lacc);
        satView_Acc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                if (isChecked){
                    PlotLineChart.lineDataSetX_Acc.setVisible(true);
                    PlotLineChart.lineDataSetY_Acc.setVisible(true);
                    PlotLineChart.lineDataSetZ_Acc.setVisible(true);
                }else{
                    PlotLineChart.lineDataSetX_Acc.setVisible(false);
                    PlotLineChart.lineDataSetY_Acc.setVisible(false);
                    PlotLineChart.lineDataSetZ_Acc.setVisible(false);
                }
            }
        });

        CheckBox satView_Gyr = (CheckBox) findViewById(R.id.cbox_gyro);
        satView_Gyr.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                if (isChecked){
                    PlotLineChart.lineDataSetX_Gyr.setVisible(true);
                    PlotLineChart.lineDataSetY_Gyr.setVisible(true);
                    PlotLineChart.lineDataSetZ_Gyr.setVisible(true);
                }else{
                    PlotLineChart.lineDataSetX_Gyr.setVisible(false);
                    PlotLineChart.lineDataSetY_Gyr.setVisible(false);
                    PlotLineChart.lineDataSetZ_Gyr.setVisible(false);
                }
            }
        });
    }
    public void buttonResetPositionClick(View view) throws IOException {
        LPLinPos[0] = -5.0d;
        LPLinPos[1] = 5.0d;
        LPLinPos[2] = 0;
    }
    public void buttonSaveClick(View view) throws IOException {
        float[] mark_data = new float[6];
        EditText editText = (EditText) findViewById(R.id.ed_txt_data_count);
        if (editText.getText().toString().isEmpty()){
            Toast.makeText(getApplicationContext(), "請填入儲存資料長度", Toast.LENGTH_SHORT).show();
        }else{
            if(chart instanceof LineChart){
                LineData lineData = ((LineChart) chart).getLineData();
                List<ILineDataSet> dataSetList = lineData.getDataSets();
                for(int j = 0; j < data_saved_cnt; j++){
                    for (int i = 0; i < dataSetList.size(); i++){
                        LineDataSet dataSet = (LineDataSet) dataSetList.get(i);
                        float y = dataSet.getValues().get((int) MyMarkerView.mark_entry.getX()+j).getY();
                        mark_data[i] = y;
                        String data = String.valueOf(mark_data[i]);
                        try {
                            fileOutputStream_txt.write(data.getBytes());
                            fileOutputStream_txt.write(String.format(",").getBytes());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    fileOutputStream_txt.write(10);
                }
                /*try {
                    fileOutputStream_txt.write(String.format("%s\n",label_number).getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
                Toast.makeText(getApplicationContext(), "資料已儲存", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class toggleButton_OnCheckedChangeListener implements CompoundButton.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            EditText editText = (EditText) findViewById(R.id.ed_txt_data_count);
            data_saved_cnt = Integer.valueOf(editText.getText().toString()).intValue();
            if(isChecked) {
                aBoolean_LineChart = true;
            }
            else {
                aBoolean_LineChart = false;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gatt_services, menu);
        if (mConnected) {
            menu.findItem(R.id.menu_connect).setVisible(false);
            menu.findItem(R.id.menu_disconnect).setVisible(true);
        } else {
            menu.findItem(R.id.menu_connect).setVisible(true);
            menu.findItem(R.id.menu_disconnect).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_connect:
                mBluetoothLeService.connect(mDeviceAddress);
                return true;
            case R.id.menu_disconnect:
                mBluetoothLeService.disconnect();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mConnectionState.setText(resourceId);
            }
        });
    }

    private void displayData(String data) {
        if (data != null) {
            mDataField.setText(data);
        }
    }

    // Demonstrates how to iterate through the supported GATT Services/Characteristics.
    // In this sample, we populate the data structure that is bound to the ExpandableListView
    // on the UI.
    private void displayGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        String uuid = null;
        String unknownServiceString = getResources().getString(R.string.unknown_service);
        String unknownCharaString = getResources().getString(R.string.unknown_characteristic);
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
                = new ArrayList<ArrayList<HashMap<String, String>>>();
        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();
            if(uuid.toLowerCase().equals(mBluetoothLeService.MSS_UUID.toString().toLowerCase())){
                currentServiceData.put(
                        LIST_NAME, SampleGattAttributes.lookup(uuid, unknownServiceString));
                currentServiceData.put(LIST_UUID, uuid);
                gattServiceData.add(currentServiceData);

                ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
                        new ArrayList<HashMap<String, String>>();
                List<BluetoothGattCharacteristic> gattCharacteristics =
                        gattService.getCharacteristics();
                ArrayList<BluetoothGattCharacteristic> charas =
                        new ArrayList<BluetoothGattCharacteristic>();

                // Loops through available Characteristics.
                for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                    charas.add(gattCharacteristic);
                    HashMap<String, String> currentCharaData = new HashMap<String, String>();
                    uuid = gattCharacteristic.getUuid().toString();
                    currentCharaData.put(
                            LIST_NAME, SampleGattAttributes.lookup(uuid, unknownCharaString));
                    currentCharaData.put(LIST_UUID, uuid);
                    gattCharacteristicGroupData.add(currentCharaData);
                }
                mGattCharacteristics.add(charas);
                gattCharacteristicData.add(gattCharacteristicGroupData);
            }
        }

        SimpleExpandableListAdapter gattServiceAdapter = new SimpleExpandableListAdapter(
                this,
                gattServiceData,
                android.R.layout.simple_expandable_list_item_2,
                new String[] {LIST_NAME, LIST_UUID},
                new int[] { android.R.id.text1, android.R.id.text2 },
                gattCharacteristicData,
                android.R.layout.simple_expandable_list_item_2,
                new String[] {LIST_NAME, LIST_UUID},
                new int[] { android.R.id.text1, android.R.id.text2 }
        );
        mGattServicesList.setAdapter(gattServiceAdapter);
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }
}
